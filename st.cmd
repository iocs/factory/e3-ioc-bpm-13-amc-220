require essioc
require adsis8300bpm

# set macros
epicsEnvSet("CONTROL_GROUP",  "PBI-BPM13")
epicsEnvSet("AMC_NAME",       "Ctrl-AMC-220")
epicsEnvSet("AMC_DEVICE",     "/dev/sis8300-5")
epicsEnvSet("EVR_NAME",       "PBI-BPM13:Ctrl-EVR-201:")
epicsEnvSet("SYSTEM1_PREFIX", "DmpL-010QC:PBI-BPM-001:")
epicsEnvSet("SYSTEM1_NAME",   "DmpL 010 QC BPM 01")
epicsEnvSet("SYSTEM2_PREFIX", "DmpL-020QC:PBI-BPM-001:")
epicsEnvSet("SYSTEM2_NAME",   "DmpL 020 QC BPM 01")

# load BPM
iocshLoad("$(adsis8300bpm_DIR)/bpm-main.iocsh", "CONTROL_GROUP=$(CONTROL_GROUP), AMC_NAME=$(AMC_NAME), AMC_DEVICE=$(AMC_DEVICE), EVR_NAME=$(EVR_NAME)")

# load common module
iocshLoad $(essioc_DIR)/common_config.iocsh

